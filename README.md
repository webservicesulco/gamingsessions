# GamingSessions

## AUTEURS
- [Deviliers Matthias](https://gitlab.com/mdevilli)
- [Ringot Arthur](https://gitlab.com/aringot1612)
- [Telliez Alexis](https://gitlab.com/alexis.telliez)

## Lien du site
<a href="https://gamingsessions.azurewebsites.net">Cliquez ici</a>

## Explication du projet

Le projet est une application qui permet de créer et/ou rejoindre des sessions de jeux.

Afin d'utiliser cette application, il est nécessaire de se connecter via un portail de login Oauth2.
Il est également possible de se connecter directement via un compte Google pour plus de rapidité.
Cette connexion Google demandera alors de faire confiance à l'application afin de récupérer les informations d'utilisateurs basiques (l'adresse mail notamment).

Dans ce cas de figure, il sera également demandé d'autoriser l'application à modifier le calendrier de l'utilisateur connecté.
- Cette fonctionnalité sera utilisée si l'utilisateur le souhaite, afin de créer un événement sur son agenda.


La fonctionnalité première de l'application est d'afficher et créer des sessions de jeux, en fournissant plusieurs informations : 
- la plateforme ;
- le nom du jeu ;
- le nombre de joueurs attendus ;
- la date et l'heure prévue.
L'utilisateur ayant créé cette session est directement récupéré via le serveur.

Nous offrons également la possibilité de modifier les sessions dont l'utilisateur connecté est le créateur.
Ce dernier a la possibilité de supprimer les sessions.

Il est possible de filtrer l'affichages des sessions en utilisant le champ de recherche de jeu rapide.

Les sessions prévues pour une date antérieur à la date du jour sont automatiquement supprimées avant affichage afin de gagner en clarté.

N'importe quel utilisateur connecté est capable de rejoindre les sessions, tant qu'il reste de la place dans le nombre de joueurs attendus.
Une fois une session rejointe, il est possible de la quitter à tout moment.

L'application permet, via des options accessibles rapidement, de réaliser deux actions différentes lorsqu'un utilisateur rejoint une session :
- Envoi automatique d'un mail de rappel
  - Un mail est directement envoyé à destination de l'adresse mail renseigné lors de la connexion à l'application.
    - Cette action, très rapide, ne nécessite pas de démarche particulière.
- Création automatique d'un événement sur le calendrier Google de l'utilisateur.
  - Cette action nécessite un compte Google. Si l'utilisateur n'a pas utilisé Google comme moyen de connexion auparavant : il sera, lors de l'action, redirigé vers la page de connexion Google.

L'application offre la possibilité d'afficher les identifiants d'utilisateurs ayant rejoint les sessions ouvertes.

Ce site web présente également en bas de page, un carrousel permettant d'afficher les dernières actualités de jeux vidéo.
Ce carrousel peut également être caché pour plus de lisibilité.

Nous avons publié notre projet sur azure : https://gamingsessions.azurewebsites.net.

## Technologies

```
Framework .NET Core : ASP.NET CORE (3.1)/ C#
Framework Javascript : React
Base de données : SQLite
Plateforme d'hébergement : Azure
Méthode d'identification : OAuth 2.0
IDE : Microsoft Visual Studio Community 2019 avec LiveShare
```

Arthur :
- ASP.NET CORE / C# : Niveau moyen, j'ai déjà réalisé un projet conséquent incluant une web api .net core (3.1).
- React / JS : Aucune expérience dans ce framework, mais utilisation d'un framework similaire (Angular) dans le cadre d'une application web.
- Azure : Aucune expérience.

Matthias :
- ASP.NET CORE / C# : J'ai déjà fait du C# pour un stage.
- React / JS : J'ai déjà fait du Javascript en L3 mais je n'ai jamais utilisé le framework React
- Azure : Aucune expérience.

Alexis : 
- ASP.NET CORE / C# : J'ai déjà fait un peu de C# lors d'un projet personnel.
- React / JS : Javascript déjà fait en Licence 3 mais ce qui est du framework React une première.
- Azure : aucune expérience.

Nous avons choisi ces langages car vous nous les avez conseillés.

## Perspectives

- Ajouter la gestion de l'âge
- Statistiques d'utilisation
- Gestion d'un bot discord pour se rejoindre dessus (serveur + ajout de rôles)
- Réseau social avec gestion d'amis.
- Affichage des streamers en fonction du jeu.
- Liaison avec les plateformes de jeux (steam, origin, epicgames, etc..)

## Annexe

### PWA
Une PWA (Progressive Web App) est une application web pouvant apparaître à l'utilisateur telle une application native sur la plateforme choisie.
En d'autres termes, l'application web peut être lancé depuis Windows en prenant la forme d’une application créée pour Windows.
Ou bien encore, elle peut être lancée via un smartphone Android afin de ressembler à une application Android native.

Ce genre d'application permet une expérience utilisateur plus agréable.
Notre site Web, utilisant React comme Framework client, offre cette fonctionnalité.

Il est ainsi possible de tester notre site web sans passer par un navigateur.
Mais cela implique une démarche supplémentaire expliquée brièvement ici : 

Afin d'utiliser notre application sous la forme d'une PWA, il suffit simplement de l'ouvrir une première fois depuis votre navigateur.

Ensuite, il faut "installer" notre pwa : cette action dépend du navigateur choisi ainsi que de sa version.

- Certains navigateurs proposent directement l'option "Installer..." dans les paramètres de la page ou bien près de la barre d'adresse ;
- D'autres navigateurs proposent, quant à elle, l'option "Installer / Créer un raccourci…", c'est le cas sur un smartphone par exemple.

Une fois l'application installée, elle sera bien souvent ouverte directement.
Afin de la retrouver, l'application sera alors considérée tel un programme sous Windows, permettant d'y accéder directement en tapant le nom de l'application en recherche rapide.
Sur smartphone, on retrouvera l'application sous la forme d'un raccourci sur la page d'accueil.

### Responsive Design

L'application est davantage adaptée à une utilisation sur ordinateur et tablette que sur smartphone, étant donné la quantité d'information à afficher.

Néanmoins, les pages sont créées afin de fournir un affichage adapté aux petits écrans, dans la mesure du possible.

### Tester l'application

Si vous souhaitez tester l'application en local, il vous suffit de :
1. Cloner le repo git du projet.
2. Ouvrir la solution avec Visual Studio.
3. Aller dans les propriétés de la solution afin de lancer plusieurs projets de démarrage :
   - gamingsessions.Api ;
   - gamingsessions.Front.
4. Démarrer la solution.
5. Se rendre à l'adresse suivante https://localhost:44300/.