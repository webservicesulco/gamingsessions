﻿import React, { Component, useState } from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useAuth0 } from "../../react-auth0-spa";
import * as sessionApi from "../../api/sessionApi";
import { Redirect } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { platforms, useStyles } from "./Settings";

export class AddSession extends Component {
    constructor(props, context) {
        super(props, context);
        this.props = props;
    }

    render() {
        return (<AddRender {...this.props} />);
    }
}

const AddRender = (props) => {
    const classes = useStyles();
    const [redirect, setRedirect] = useState(false);
    const { getTokenSilently } = useAuth0();
    const today = new Date();
    today.setTime(today.getTime() - today.getTimezoneOffset() * 60 * 1000);
    const formData = { platForm: platforms[0].name, gameName: '', playerMaxAccount: 2, sessionDate: today.toISOString().slice(0, -8) };

    const handleChange = (event) => {
        formData[event.target.id] = event.target.value;
    };

    const handleChangePlatForm = (event) => {
        formData["platForm"] = event.target.value;
    };

    const handleChangeNumber = (event) => {
        if (event.target.value >= 2) {
            formData[event.target.id] = event.target.value;
        } else event.target.value = 2;
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const token = await getTokenSilently();
        if (/\S/.test(formData['gameName'])) {
            // Ajout de la session via la web api.
            await sessionApi.createSession(token, formData);
            setRedirect(true);
        }
        else
            alert("Attention, merci de renseigner un nom de jeu valide.");
    };

    return redirect ? (<Redirect to='/fetch-session' />) : (
        <React.Fragment>
            <h1>Ajout d'une session</h1>
            <form className={classes.root} noValidate autoComplete="on" onSubmit={handleSubmit}>
                <FormControl variant="filled" className={classes.formControl}>
                    <InputLabel id="platform">Plateforme</InputLabel>
                    <Select
                        labelId="platform"
                        id="platforms"
                        label="Plateforme"
                        defaultValue={formData.platForm}
                        variant="filled"
                        onChange={(event) => {
                            handleChangePlatForm(event)
                        }}
                    >
                        {platforms.map((item) => (
                            <MenuItem key={item.name} value={item.name}>
                                {item.name}
                            </MenuItem>
                        ))}
                    </Select>
                </FormControl>
                <TextField required id="gameName" label="Nom du jeu" variant="filled" onChange={handleChange} />
                <TextField required id="playerMaxAccount" label="Nombre de joueur" type="number" defaultValue="2" patern="[0-9]" variant="filled" onChange={handleChangeNumber} />
                <TextField required
                    id="sessionDate"
                    label="Date Session"
                    variant="filled"
                    type="datetime-local"
                    defaultValue={formData.sessionDate}
                    onChange={handleChange}
                    InputLabelProps={{
                        shrink: true,
                    }}
                />
                <Button className={classes.root} type="submit" variant="contained" color="primary">Enregistrer</Button>
            </form>
        </React.Fragment>
    );
}