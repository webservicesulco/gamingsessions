﻿import React, { useState, useEffect } from "react";
import * as sessionApi from "../../api/sessionApi";
import * as userCrossSessionApi from "../../api/userCrossSessionApi";
import * as calendarApi from "../../api/calendarApi";
import * as mailApi from "../../api/mailApi";
import * as userApi from "../../api/userApi";
import { useAuth0 } from "../../react-auth0-spa";
import { Link } from 'react-router-dom';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Fab from '@material-ui/core/Fab';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import TextField from '@material-ui/core/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';

const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            borderBottom: 'unset'
        },
        flexShrink: 0,
        marginLeft: theme.spacing(2.5)
    },
    userList: {
        borderBottom: 0
    },
    margin: {
        margin: theme.spacing(1)
    },
    textField: {
        margin: theme.spacing(1),
        display: 'flex'
    },
    flexBox: {
        display: 'flex'
    },
    formControl: {
        margin: theme.spacing(3)
    },
    table: {
        minWidth: 500,
    }
}));

export const FetchSessions = () => {
    const { getTokenSilently } = useAuth0();
    const [loading, setLoading] = useState(true);
    const [sessions, setSessions] = useState([]);
    const [backupSessions, setBackupSessions] = useState([]);
    const [user, setUser] = useState({});
    const [optionForm, setOptionForm] = useState({ email: true, calendarEvent: false });
    const classes = useStyles();
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(7);

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };

    const populateData = async () => {
        const token = await getTokenSilently();
        // Suppression des liaisons utilisateurs - sessions obsolétes.
        await userCrossSessionApi.deleteOutDateSessionEntries(token);
        // Suppression des sessions obsolétes.
        await sessionApi.deleteOutDateSession(token);
        // Récupération de toutes les sessions.
        const data = await sessionApi.get(token);
        for (let i = 0; i < data.length; i++) {
            // Récupération de la liste des utilisateurs ayant rejoint chaque session.
            data[i].players = await userCrossSessionApi.GetUsersBySession(token, data[i].id);
        }
        // Récupération des informations utilisateurs et des sessions qu'il a rejoint.
        const userData = await userApi.get(token);
        if (userData !== undefined)
            userData.sessions = await userCrossSessionApi.getSessionsByUser(token);
        setSessions(data);
        setBackupSessions(data);
        setUser(userData);
        setLoading(false);
    };

    const handleChange = async (event) => {
        let filteredSessions = backupSessions.filter(value => value.gameName.toLowerCase().includes(event.target.value.toLowerCase()));
        setSessions(filteredSessions);
    };

    const handleOptionsChanges = (event) => {
        setOptionForm({ ...optionForm, [event.target.name]: event.target.checked });
    };

    useEffect(() => { populateData(); }, []); // eslint-disable-line react-hooks/exhaustive-deps

    const joinSession = async (idSession) => {
        // Fait appel à la web api, permettant à l'utilisateur de rejoindre la session choisie.
        const token = await getTokenSilently();
        await userCrossSessionApi.joinSession(token, idSession);
        // Envoi d'un mail et création d'un événement Google Calendar selon les options choisies.
        if (optionForm.email)
            mailApi.send(token, idSession);
        if (optionForm.calendarEvent)
            calendarApi.send(idSession);
        // Mise à jour du tableau.
        populateData();
    };

    const deleteSession = async (idSession) => {
        // Fait appel à la web api, permettant de supprimer la session choisie.
        const token = await getTokenSilently();
        // Les liaisons utilisateurs - sessions sont également supprimées.
        await userCrossSessionApi.deleteBySession(token, idSession);
        await sessionApi.deleteSession(token, idSession);
        populateData();
    };

    const quitSession = async (idSession) => {
        // Fait appel à la web api, permettant à l'utilisateur de quitter la session choisie.
        const token = await getTokenSilently();
        await userCrossSessionApi.quitSession(token, idSession);
        populateData();
    };

    function TablePaginationActions(props) {
        const classes = useStyles();
        const theme = useTheme();
        const { count, page, rowsPerPage, onChangePage } = props;

        const handleFirstPageButtonClick = (event) => {
            onChangePage(event, 0);
        };

        const handleBackButtonClick = (event) => {
            onChangePage(event, page - 1);
        };

        const handleNextButtonClick = (event) => {
            onChangePage(event, page + 1);
        };

        const handleLastPageButtonClick = (event) => {
            onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
        };

        return (
            <div className={classes.root}>
                <IconButton
                    onClick={handleFirstPageButtonClick}
                    disabled={page === 0}
                    aria-label="first page"
                >
                    {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
                </IconButton>
                <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
                    {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
                </IconButton>
                <IconButton
                    onClick={handleNextButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="next page"
                >
                    {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
                </IconButton>
                <IconButton
                    onClick={handleLastPageButtonClick}
                    disabled={page >= Math.ceil(count / rowsPerPage) - 1}
                    aria-label="last page"
                >
                    {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
                </IconButton>
            </div>
        );
    }

    function Row(props) {
        const { session } = props;
        const [open, setOpen] = React.useState(false);
        const editUrl = "/edit-session/" + session.id;

        return (
            <React.Fragment>
                <TableRow key={session.creator.userName} className={classes.root}>
                    <TableCell>
                        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)} disabled={session.players.length <= 0}>
                            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
                        </IconButton>
                    </TableCell>
                    <TableCell className="d-none d-xxs-table-cell" scope="row">{session.creator.userName}</TableCell>
                    <TableCell align="right" className="d-none d-xxs-table-cell">{session.platForm}</TableCell>
                    <TableCell align="right">{session.gameName}</TableCell>
                    <TableCell align="right" className="d-none d-xxxxxxxs-table-cell">{session.playerAccount}/{session.playerMaxAccount}</TableCell>
                    <TableCell align="right" className="d-none d-xxs-table-cell">{new Intl.DateTimeFormat('fr-FR', { dateStyle: 'full', timeStyle: 'long' }).format(new Date(session.sessionDate)).slice(0, -9)}</TableCell>
                    {user.sessions.includes(session.id) ?
                        (<TableCell align="center"><Button variant="contained" color="secondary" onClick={() => quitSession(session.id)} >Quitter</Button></TableCell>)
                        :
                        (<TableCell align="center"><Button variant="contained" color="primary" onClick={() => joinSession(session.id)} disabled={session.playerAccount >= session.playerMaxAccount}>Rejoindre</Button></TableCell>)
                    }
                    {user.id === session.creator.id ?
                        (<TableCell align="center"><Fab size="small" aria-label="éditer" className={classes.margin} component={Link} to={editUrl}><EditIcon /></Fab><Fab size="small" aria-label="supprimer" className={classes.margin} onClick={() => deleteSession(session.id)}><DeleteIcon /></Fab></TableCell>)
                        :
                        (<TableCell align="center"></TableCell>)
                    }
                </TableRow>
                <TableRow>
                    <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={8}>
                        <Collapse in={open} timeout="auto" unmountOnExit>
                            <Box margin={1}>
                                <Typography variant="h6" gutterBottom component="div">
                                    Joueur(s) présent(s)
                                </Typography>
                                <Table size="small">
                                    <TableBody>
                                        {
                                            session.players.map((player) => (
                                                <TableRow key={session.gameName + player}>
                                                    <TableCell component="th" scope="row">
                                                        {player}
                                                    </TableCell>
                                                </TableRow>
                                            ))
                                        }
                                    </TableBody>
                                </Table>
                            </Box>
                        </Collapse>
                    </TableCell>
                </TableRow>
            </React.Fragment>
        );
    }
    const renderTable = (sessions) => {
        return (
            <div>
                <h1 id="tabelLabel" >Sessions</h1>
                <Button style={{ display: 'flex', height: '7ch' }} variant="contained" size="large" color="primary" component={Link} to="/add-session">Ajouter une session</Button>
                <TextField className={classes.textField} id="gameSearch" label="Recherche de jeu rapide" onChange={handleChange} />
                <TableContainer component={Paper}>
                    <Table aria-label="collapsible table">
                        <TableHead>
                            <TableRow>
                                <TableCell />
                                <TableCell className="d-none d-xxs-table-cell">Créateur de la session</TableCell>
                                <TableCell className="d-none d-xxs-table-cell" align="right">Plateforme</TableCell>
                                <TableCell align="right">Nom du jeu</TableCell>
                                <TableCell className="d-none d-xxxxxxxs-table-cell" align="right">Nombre de joueurs</TableCell>
                                <TableCell className="d-none d-xxs-table-cell" align="right">Date de session prévue</TableCell>
                                <TableCell align="center"></TableCell>
                                <TableCell align="center"></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {(rowsPerPage > 0
                                ? sessions.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                : sessions
                            ).map((session) => (
                                <Row key={session.id} session={session} />
                            ))}
                        </TableBody>
                        <TableFooter>
                            <TableRow>
                                <TablePagination
                                    rowsPerPageOptions={[5, 7, 10, 25, { label: 'All', value: -1 }]}
                                    count={sessions.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    onChangePage={handleChangePage}
                                    onChangeRowsPerPage={handleChangeRowsPerPage}
                                    ActionsComponent={TablePaginationActions}
                                />
                            </TableRow>
                        </TableFooter>
                    </Table>
                </TableContainer>
                <div className={classes.flexBox}>
                    <FormControl className={classes.formControl}>
                        <FormLabel >Options : Rappel de session </FormLabel>
                        <FormGroup>
                            <FormControlLabel
                                control={<Checkbox checked={optionForm.email} onChange={handleOptionsChanges} name="email" />}
                                label="Envoi d'un mail de rappel"
                            />
                            <FormControlLabel
                                control={<Checkbox checked={optionForm.calendarEvent} onChange={handleOptionsChanges} name="calendarEvent" />}
                                label="Ajout d'un événement sur Google Calendar"
                            />
                        </FormGroup>
                    </FormControl>
                </div>
            </div>
        );
    };

    let contents = loading ? (<p><em>Loading...</em></p>) : (renderTable(sessions));

    return (contents);
};