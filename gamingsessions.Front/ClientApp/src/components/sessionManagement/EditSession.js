﻿import React, { Component, useState, useEffect } from "react";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { useAuth0 } from "../../react-auth0-spa";
import * as sessionApi from "../../api/sessionApi";
import { Redirect } from "react-router-dom";
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import { platforms, useStyles } from "./Settings";

export class EditSession extends Component {
    constructor(props, context) {
        super(props, context);
        this.props = props;
    }

    render() {
        return (<AddRender {...this.props} />);
    }
}

const AddRender = (props) => {
    const classes = useStyles();
    const [redirect, setRedirect] = useState(false);
    const { getTokenSilently } = useAuth0();
    const today = new Date();
    const [loading, setLoading] = useState(true);
    today.setTime(today.getTime() - today.getTimezoneOffset() * 60 * 1000);
    const [formData, setFormData] = useState({ platForm: '', gameName: '', playerMaxAccount: 2, sessionDate: today.toISOString().slice(0, -8) });
    const idSession = props.match.params.idSession;

    const getData = async () => {
        // Récupération des informations de la session à modifier.
        const token = await getTokenSilently();
        const data = await sessionApi.getSession(token, idSession);
        setFormData(data);
        setLoading(false);
    }

    useEffect(() => { getData(); }, []);  // eslint-disable-line react-hooks/exhaustive-deps

    const handleChangePlatForm = (event) => {
        formData["platForm"] = event.target.value;
    };

    const handleChange = (event) => {
        formData[event.target.id] = event.target.value;
    };

    const handleChangeNumber = (event) => {
        if (event.target.value >= 2) {
            formData[event.target.id] = event.target.value;
        } else event.target.value = 2;
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        const token = await getTokenSilently();
        if (/\S/.test(formData['gameName'])) {
            // Modification de la session via la web api.
            await sessionApi.updateSession(token, formData);
            setRedirect(true);
        }
        else
            alert("Attention, merci de renseigner un nom de jeu valide.");
    };

    const renderForm = (formData) => {
        return redirect ? (<Redirect to='/fetch-session' />) :
            (<React.Fragment>
                <h1>Modification d'une session</h1>
                <form className={classes.root} noValidate autoComplete="on" onSubmit={handleSubmit}>
                    <FormControl variant="filled" className={classes.formControl}>
                        <InputLabel id="platform">Plateforme</InputLabel>
                        <Select
                            labelId="platform"
                            id="platform"
                            label="Plateforme"
                            defaultValue={formData.platForm}
                            variant="filled"
                            onChange={(event) => {
                                handleChangePlatForm(event)
                            }}
                        >
                            {platforms.map((item) => (
                                <MenuItem key={item.name} value={item.name}>
                                    {item.name}
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                    <TextField required id="gameName" defaultValue={formData.gameName} label="gameName" variant="filled" onChange={handleChange} />
                    <TextField required id="playerMaxAccount" defaultValue={formData.playerMaxAccount} label="Nombre de joueur" type="number" patern="[0-9]" variant="filled" onChange={handleChangeNumber} />
                    <TextField required
                        id="sessionDate"
                        label="Date Session"
                        variant="filled"
                        type="datetime-local"
                        defaultValue={formData.sessionDate}
                        onChange={handleChange}
                        InputLabelProps={{
                            shrink: true,
                        }}
                    />
                    <Button className={classes.root} type="submit" variant="contained" color="primary">Enregistrer</Button>
                </form>
            </React.Fragment>);
    }

    let contents = loading ? (<p></p>) : (renderForm(formData));
    return (contents);
}