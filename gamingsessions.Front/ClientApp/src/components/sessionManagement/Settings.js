import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles((theme) => ({
    root: {
        '& > *': {
            margin: theme.spacing(1),
            width: '40ch',
        },
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        'flex-flow': 'wrap'
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120,
    }
}));

export const platforms = [
    { name: 'PC/MAC' },
    { name: 'XBOX SERIES' },
    { name: 'XBOX ONE' },
    { name: 'PS5' },
    { name: 'PS4' },
    { name: 'SWITCH' },
    { name: 'Cross Platform' }
];