import React, { useState, useEffect } from "react";
import { Container } from "reactstrap";
import { makeStyles } from '@material-ui/core/styles';
import Carousel from 'react-multi-carousel';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActionArea from '@material-ui/core/CardActionArea';
import Typography from '@material-ui/core/Typography';
import * as gameNewsApi from '../api/gameNewsApi';
import 'react-multi-carousel/lib/styles.css';
import "./Footer.css";
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import AnnouncementIcon from '@material-ui/icons/Announcement';
import Fab from '@material-ui/core/Fab';

const useStyles = makeStyles((theme) => ({
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    extendedIcon: {
        marginBottom: theme.spacing(2),
        float: 'right'
    },
    carousel: {
        clear: 'both'
    }
}));

const responsive = {
    superLargeDesktop: {
        breakpoint: { max: 4096, min: 3440 },
        items: 7
    },
    largeDesktop: {
        breakpoint: { max: 3440, min: 2048 },
        items: 6
    },
    desktop: {
        breakpoint: { max: 2048, min: 1024 },
        items: 4
    },
    tablet: {
        breakpoint: { max: 1024, min: 464 },
        items: 2
    },
    mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 0
    }
};

export const Footer = () => {
    const [loading, setLoading] = useState(true);
    const [news, setNews] = useState([]);
    const [activate, setActivate] = useState(true);
    const classes = useStyles();

    const populateData = async () => {
        // Récupération des news via le serveur.
        const data = await gameNewsApi.getNews();
        setNews(data);
        setLoading(false);
    };

    const handleChange = () => {
        setActivate(!activate);
    }

    useEffect(() => { populateData(); }, []);

    const Item = (article) => {
        return (
            <Card style={{ backgroundImage: `url(${article.item.urlToImage})`, backgroundSize: 'cover', height: '14em' }}>
                <CardActionArea style={{ backgroundColor: 'rgba(255,255,255,0.6)', height: '100%', textDecoration: 'none' }} href={article.item.url}>
                    <CardContent >
                        <Typography className={classes.title} color="textPrimary" gutterBottom>
                            {article.item.author}
                        </Typography>
                        <Typography variant="h6" component="h6" >
                            {article.item.title}
                        </Typography>
                        <Typography variant="body2" component="p">
                            {article.item.description}
                        </Typography>
                    </CardContent>
                </CardActionArea>
            </Card>
        )
    }
    const render = (news) => {
        return (
            window.innerWidth > 720 ? (
                <footer className="footer">
                    {activate ? (
                        <Container fluid={true}>
                            <Fab variant="extended" className={classes.extendedIcon} onClick={handleChange}>
                                <HighlightOffIcon />Desactiver les news
                        </Fab>
                            <Carousel responsive={responsive} infinite={true} ssr={true} autoPlay={true} containerClass="carousel-container" className={classes.carousel}>
                                {
                                    news.map((article, i) => <Item key={i} item={article} />)
                                }
                            </Carousel>
                        </Container>
                    ) : (
                            <Container fluid={true}>
                                <Fab variant="extended" className={classes.extendedIcon} onClick={handleChange}>
                                    <AnnouncementIcon />Activer les news
                        </Fab>
                            </Container>)}
                </footer>
            ) : (<footer></footer>)
        );
    };

    let contents = loading ? (<p></p>) : (render(news));

    return (contents);
};