import React, { useState } from "react";
import {
    Button,
    Collapse,
    Container,
    Navbar,
    NavbarBrand,
    NavbarToggler,
    NavItem,
    NavLink,
} from "reactstrap";
import { Link } from "react-router-dom";
import "./NavMenu.css";
import { useAuth0 } from "../react-auth0-spa";
import logo from '../icons/favicon.ico';

export const NavMenu = () => {
    const { isAuthenticated, loginWithRedirect, logout } = useAuth0();
    const [collapsed, setCollapsed] = useState(true);

    const toggleNavbar = () => {
        setCollapsed(!collapsed);
    };

    return (
        <header>
            <Navbar className="navbar-expand-sm navbar-toggleable-sm ng-white border-bottom box-shadow mb-3" light>
                <Container>
                    <NavbarBrand tag={Link} to="/">
                        <img className="img" src={logo} alt="Logo" />
                        Gaming Sessions
                    </NavbarBrand>
                    <NavbarToggler onClick={toggleNavbar} className="mr-2" />
                    <Collapse className="d-sm-inline-flex flex-sm-row-reverse" isOpen={!collapsed} navbar>
                        <ul className="navbar-nav flex-grow">
                            <NavItem>
                                <NavLink tag={Link} className="text-dark" to="/">Page d'accueil</NavLink>
                            </NavItem>
                            {isAuthenticated && (
                                <>
                                    <NavItem>
                                        <NavLink tag={Link} className="text-dark" to="/fetch-session">Affichage des sessions</NavLink>
                                    </NavItem>
                                    <NavItem>
                                        <Button className="btn btn-danger btn-block" size="sml" onClick={() => logout({})}>
                                            Deconnexion
                                        </Button>
                                    </NavItem>
                                </>
                            )}

                            {!isAuthenticated && (
                                <NavItem>
                                    <Button className="btn btn-primary btn-block" size="sml" onClick={() => loginWithRedirect({})}>
                                        Connexion
                                    </Button>
                                </NavItem>
                            )}
                        </ul>
                    </Collapse>
                </Container>
            </Navbar>
        </header>
    );
}