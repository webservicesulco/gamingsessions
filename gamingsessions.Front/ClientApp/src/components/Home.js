import React, { useRef } from "react";
import { useAuth0 } from "../react-auth0-spa";
import * as userApi from "../api/userApi";
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';

export const Home = () => {
    const { isAuthenticated, user, getTokenSilently, loginWithRedirect } = useAuth0();
    const ref = useRef(false);
    const checkConnectedUser = async () => {
        if (isAuthenticated && user !== undefined) {
            ref.current = true;
            const data = { userName: ((user.given_name === undefined) ? user.name : user.nickname), oAuthId: user.sub, email: user.email };
            const token = await getTokenSilently();
            // On cherche ici à vérifier si l'utilisateur connecté existe déjà dans la base de données.
            let userExist = await userApi.get(token)
            // S'il n'existe pas, on le créé.
            if (userExist === undefined)
                await userApi.addUser(token, data);
        }
    };

    if (!ref.current) { checkConnectedUser(); }

    if (isAuthenticated && user !== undefined)
        return (
            <div>
                <h1>Bienvenue {(user.given_name === undefined) ? user.name : user.nickname} </h1>
                <Button variant="contained" color="primary" component={Link} to="/fetch-session">Cliquez ici pour afficher les sessions</Button>
            </div>
        );
    else
        return (
            <div>
                <h1>Bienvenue</h1>
                <Button variant="contained" color="primary" onClick={() => loginWithRedirect({})}>Cliquez ici pour vous connecter</Button>
            </div>
        );
};