import React from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Switch } from "react-router-dom";
import { Home } from './components/Home';
import { FetchSessions } from './components/sessionManagement/FetchSessions';
import { AddSession } from './components/sessionManagement/AddSession';
import { EditSession } from './components/sessionManagement/EditSession';
import PrivateRoute from "./utils/PrivateRoute";
import './custom.css'

require('dotenv').config()

const App = () => {
    return (
        <Layout>
            <Switch>
                <Route exact path='/' component={Home} />
                <PrivateRoute path='/fetch-session' component={FetchSessions} />
                <PrivateRoute path='/add-session' component={AddSession} />
                <PrivateRoute path='/edit-session/:idSession' component={EditSession} />
            </Switch>
        </Layout>
    );
};

export default App;