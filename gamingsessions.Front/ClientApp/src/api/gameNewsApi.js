﻿import { handleJsonResponse, handleError, apiUrl } from "./apiUtils";

const baseUrl = apiUrl + "/gamenews";

/** Permet de récupérer les news en appelant la web api. */
export async function getNews() {
    try {
        const response = await fetch(baseUrl, {
            method: 'GET',
            headers: { Accept: "application/json" }
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}