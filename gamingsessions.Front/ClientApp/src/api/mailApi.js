﻿import { handleEmptyResponse, handleError, apiUrl } from "./apiUtils";
const baseUrl = apiUrl + "/mail";

/**
 * Permet d'envoyer un mail en appelant la web api.
 * @param {any} token Access token à transmettre pour la web api.
 * @param {any} idSession Identifiant de la session concernée par le mail.
 */
export async function send(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/send/" + idSession, {
            method: 'PUT',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}