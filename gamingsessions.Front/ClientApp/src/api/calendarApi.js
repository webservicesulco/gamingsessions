/**
 * Permet d'appeler la web api afin que cette derni�re cr�er un �v�nement sur le calendrier de l'utilisateur.
 * Cette action doit �tre r�alis�e pour le moment via une redirection.
 * Cela implique une requ�te de type GET.
 * @param {any} idSession La session que l'utilisateur cherche � rejoindre.
 */
export async function send(idSession) {
    window.location.href = process.env.REACT_APP_API_URL + "/calendar/send/" + idSession;
}