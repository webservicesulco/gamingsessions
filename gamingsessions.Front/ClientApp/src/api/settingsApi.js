import { handleJsonResponse, handleError, apiUrl } from "./apiUtils";

const baseUrl = apiUrl + "/settings/auth";

/** Permet de r�cup�rer les informations Oauth2 h�berg�es sur le serveur. */
export async function getAuthSettings() {
    try {
        const response = await fetch(baseUrl, {
            method: 'GET',
            headers: { Accept: "application/json" }
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}