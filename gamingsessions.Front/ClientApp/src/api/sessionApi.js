import { handleJsonResponse, handleEmptyResponse, handleError, apiUrl } from "./apiUtils";
const baseUrl = apiUrl + "/sessions";

/**
 * Permet de r�cup�rer toutes les sessions en appelant la web api.
 * @param {any} token Access token � transmettre pour la web api.
 */
export async function get(token) {
    try {
        const response = await fetch(baseUrl, {
            method: 'GET',
            headers: { Accept: "application/json", Authorization: `Bearer ${token}` }
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de cr�er une session en appelant la web api.
 * @param {any} token Access token � transmettre pour la web api.
 * @param {any} formData Formulaire de cr�ation de session.
 */
export async function createSession(token, formData) {
    try {
        const response = await fetch(baseUrl, {
            method: 'PUT',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` },
            body: JSON.stringify(formData)
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de r�cup�rer une session gr�ce � son ID en appelant la web api.
 * @param {any} token Access token � transmettre pour la web api.
 * @param {any} idSession ID de la session � r�cup�rer.
 */
export async function getSession(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/" + idSession, {
            method: 'GET',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de mettre � jour une session en appelant la web api.
 * @param {any} token Access token � transmettre pour la web api.
 * @param {any} formData Formulaire contenant la session mise � jour.
 */
export async function updateSession(token, formData) {
    try {
        const response = await fetch(baseUrl, {
            method: 'POST',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` },
            body: JSON.stringify(formData)
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de supprimer une session gr�ce � son ID en appelant la web api.
 * @param {any} token Access token � transmettre pour la web api.
 * @param {any} idSession ID de la session � supprimer.
 */
export async function deleteSession(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/" + idSession, {
            method: 'DELETE',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de supprimer les sessions obsol�tes en appelant la web api.
 * @param {any} token Access token � transmettre pour la web api.
 */
export async function deleteOutDateSession(token) {
    try {
        const response = await fetch(baseUrl + "/outDate", {
            method: 'DELETE',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}