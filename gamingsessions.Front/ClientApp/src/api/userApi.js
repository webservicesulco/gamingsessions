﻿import { handleJsonResponse, handleSpecialResponse, handleError, apiUrl } from "./apiUtils";

const baseUrl = apiUrl + "/users";

/**
 * Permet d'ajouter un utilisateur en apppelant la web api.
 * @param {any} token Access token à transmettre pour la web api.
 * @param {any} formData Formulaire contenant l'utilisateur à ajouter.
 */
export async function addUser(token, formData) {
    try {
        const response = await fetch(baseUrl, {
            method: 'PUT',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` },
            body: JSON.stringify(formData)
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Récupération du compte de l'utilisateur en appelant la web api.
 * On s'attend à ce que l'utilisateur ne soit pas encore connu vis à vis de la base de données.
 * @param {any} token Access token à transmettre pour la web api.
 */
export async function get(token) {
    const response = await fetch(baseUrl + "/account", {
        method: 'GET',
        headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
    });
    return handleSpecialResponse(response);
}