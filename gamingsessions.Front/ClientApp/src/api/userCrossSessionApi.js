﻿import { handleError, apiUrl, handleEmptyResponse, handleJsonResponse } from "./apiUtils";
const baseUrl = apiUrl + "/usercrosssession";

/**
 * Permet à l'utilisateur connecté de rejoindre une session concernée en faisant appel à la web api.
 * @param {any} token Access token à transmettre pour la web api.
 * @param {any} idSession L'ID de la session à rejoindre.
 */
export async function joinSession(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/" + idSession, {
            method: 'PUT',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet à l'utilisateur connecté de quitter la session concernée en faisant appel à la web api.
 * @param {any} token Access token à transmettre pour la web api.
 * @param {any} idSession L'ID de la session à quitter.
 */
export async function quitSession(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/" + idSession, {
            method: 'DELETE',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de récupérer les sessions rejointes par l'utilisateur connecté en faisant appel à la web api.
 * @param {any} token Access token à transmettre pour la web api.
 */
export async function getSessionsByUser(token) {
    try {
        const response = await fetch(baseUrl + "/byUser", {
            method: 'GET',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de récupérer les utilisateurs ayant rejoint la session concernée en faisant appel à la web api.
 * @param {any} token Access token à transmettre pour la web api.
 * @param {any} idSession ID de la session concernée.
 */
export async function GetUsersBySession(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/bySession/" + idSession, {
            method: 'GET',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleJsonResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de supprimer les liaisons utilisateurs - sessions rattachées à une session concernée en faisant appel à la web api.
 * @param {any} token Access token à transmettre pour la web api.
 * @param {any} idSession ID de la session concernée.
 */
export async function deleteBySession(token, idSession) {
    try {
        const response = await fetch(baseUrl + "/bySession/" + idSession, {
            method: 'DELETE',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}

/**
 * Permet de supprimer les liaisons utilisateurs - sessions obsolétes en faisant appel à la web api.
 * @param {any} token Access token à transmettre pour la web api.
 */
export async function deleteOutDateSessionEntries(token) {
    try {
        const response = await fetch(baseUrl + "/outDate", {
            method: 'DELETE',
            headers: { "Content-Type": "application/json", Authorization: `Bearer ${token}` }
        });
        return handleEmptyResponse(response);
    } catch (error) {
        handleError(error);
    }
}