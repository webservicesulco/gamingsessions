/**
 *  Permet de traiter une r�ponse au format json.
 * @param {any} response R�ponse du serveur � traiter.
 */
export async function handleJsonResponse(response) {
    if (response.ok) {
        return response.json();
    } else {
        throw new Error(`Error calling api - ${response.statusText}`);
    }
}

/**
 * Permet de traiter une r�ponse sp�cifique;
 * On attend, soit des informations sur l'utilisateur au format json,
 * Soit un utilisateur inconnu.
 * @param {any} response R�ponse du serveur � traiter.
 */
export async function handleSpecialResponse(response) {
    if (response.ok)
        return response.json();
    else
        return undefined;
}

/**
 * Permet de traiter une r�ponse au format texte.
 * @param {any} response R�ponse du serveur � traiter.
 */
export async function handleTextResponse(response) {
    if (response.ok) {
        return response.text();
    } else {
        throw new Error(`Error calling api - ${response.statusText}`);
    }
}

/**
 * Permet de traiter une r�ponse vide (sans contenu attendu).
 * @param {any} response R�ponse du serveur � traiter.
 */
export async function handleEmptyResponse(response) {
    if (response.ok || response.noContent || response.accepted) {
        return true;
    } else {
        throw new Error(`Error calling api - ${response.statusText}`);
    }
}

/**
 * Permet de traiter les erreurs serveurs.
 * @param {any} error R�ponse du serveur � traiter.
 */
export function handleError(error) {
    console.error("API call failed. " + error);
    throw error;
}

// Url de la web api.
export const apiUrl = process.env.REACT_APP_API_URL;