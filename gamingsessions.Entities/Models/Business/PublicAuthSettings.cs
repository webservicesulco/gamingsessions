namespace gamingsessions.Entities.Models.Business
{
    public class PublicAuthSettings
    {
        public string Domain { get; set; }
        public string ClientId { get; set; }
        public string Audience { get; set; }
    }
}