﻿using System;

namespace gamingsessions.Entities.Models.Business
{
    public class Calendar
    {
        public string Summary { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
    }
}