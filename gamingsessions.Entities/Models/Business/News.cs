﻿using System.Collections.Generic;

namespace gamingsessions.Entities.Models.Business
{
    public class News
    {
        public string Status { get; set; }
        public int TotalResults { get; set; }
        public List<Article> Articles { get; set; }
    }
}