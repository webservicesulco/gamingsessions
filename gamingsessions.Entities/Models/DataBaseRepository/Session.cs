﻿namespace gamingsessions.Entities.Models.DataBaseRepository
{
    public class Session
    {
        public int Id { get; set; }
        public User Creator { get; set; }
        public string PlatForm { get; set; }
        public string GameName { get; set; }
        public int PlayerAccount { get; set; }
        public int PlayerMaxAccount { get; set; }
        public string CreationDate { get; set; }
        public string SessionDate { get; set; }
    }
}