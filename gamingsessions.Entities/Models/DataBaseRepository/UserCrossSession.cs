﻿namespace gamingsessions.Entities.Models.DataBaseRepository
{
    public class UserCrossSession
    {
        public int Id { get; set; }
        public User User { get; set; }
        public Session Session { get; set; }
    }
}