﻿namespace gamingsessions.Entities.Models.DataBaseRepository
{
    public class User
    {
        public int Id { get; set; }
        public string OAuthId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
    }
}