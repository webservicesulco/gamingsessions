﻿using gamingsessions.Entities.Models.DataBaseRepository;
using System.Collections.Generic;

namespace gamingsessions.Core.Interfaces.DataBaseRepository
{
    public interface IUserCrossSessionRepository
    {
        IEnumerable<int> SelectSessionsByUser(User user);

        IEnumerable<string> SelectUsersBySession(Session session);

        bool Insert(User user, Session session);

        bool Delete(User user, Session session);

        bool DeleteBySession(Session sesion);

        bool DeleteOutDate();
    }
}