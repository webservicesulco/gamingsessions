﻿using gamingsessions.Entities.Models.DataBaseRepository;
using System.Collections.Generic;

namespace gamingsessions.Core.Interfaces.DataBaseRepository
{
    public interface ISessionRepository
    {
        Session Select(int id);

        IEnumerable<Session> SelectAll();

        Session Insert(Session session);

        Session Update(int id, Session session);

        bool Delete(int idSession);

        bool DeleteOutDate();
    }
}