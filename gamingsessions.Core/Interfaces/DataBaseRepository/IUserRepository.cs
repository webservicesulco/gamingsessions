﻿using gamingsessions.Entities.Models.DataBaseRepository;

namespace gamingsessions.Core.Interfaces.DataBaseRepository
{
    public interface IUserRepository
    {
        User Select(int id);

        User SelectByOAuthID(string oAuthID);

        User Insert(User user);
    }
}