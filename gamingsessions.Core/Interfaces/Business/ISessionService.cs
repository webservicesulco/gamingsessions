﻿using gamingsessions.Entities.Models.DataBaseRepository;
using System.Collections.Generic;

namespace gamingsessions.Core.Interfaces.Business
{
    public interface ISessionService
    {
        Session Retrieve(int id);

        IEnumerable<Session> RetrieveAll();

        Session Update(int id, Session session);

        Session Create(string oAuthID, Session session);

        bool Remove(int idSession);

        bool RemoveOutDate();
    }
}