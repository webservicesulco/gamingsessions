﻿using System.Threading.Tasks;

namespace gamingsessions.Core.Interfaces.Business
{
    public interface IMailService
    {
        Task SendEmailAsync(int idSession, string oAuthID);
    }
}