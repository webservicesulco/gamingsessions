﻿using System.Collections.Generic;

namespace gamingsessions.Core.Interfaces.Business
{
    public interface IUserCrossSessionService
    {
        bool Create(string oAuthID, int idSession);

        IEnumerable<int> GetSessionsByUser(string oAuthID);

        IEnumerable<string> GetUsersBySession(int idSession);

        bool Remove(string oAuthID, int idSession);

        bool RemoveBySession(int idSession);

        bool RemoveOutDate();
    }
}