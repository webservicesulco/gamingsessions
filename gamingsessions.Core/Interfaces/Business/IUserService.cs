﻿using gamingsessions.Entities.Models.DataBaseRepository;

namespace gamingsessions.Core.Interfaces.Business
{
    public interface IUserService
    {
        User RetrieveByOAuthID(string oAuthID);

        User Create(User user);
    }
}