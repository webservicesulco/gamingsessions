﻿using gamingsessions.Entities.Models.Business;
using System.Collections.Generic;

namespace gamingsessions.Core.Interfaces.Business
{
    public interface IGameNewsService
    {
        List<Article> GetGameNews();
    }
}