﻿using Google.Apis.Auth.OAuth2;

namespace gamingsessions.Core.Interfaces.Business
{
    public interface ICalendarService
    {
        bool CreateEvent(GoogleCredential cred, int idSession);
    }
}