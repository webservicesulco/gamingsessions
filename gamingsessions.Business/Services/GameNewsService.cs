﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Entities.Models.Business;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;

namespace gamingsessions.Business.Services
{
    /** Classe permettant de récupérer les actualités de jeux. */
    public class GameNewsService : IGameNewsService
    {
        private readonly string apiUrl;

        public GameNewsService(IConfiguration configuration)
        {
            this.apiUrl = configuration.GetValue<string>("NewsApiSettings:Url")
                        + configuration.GetValue<string>("NewsApiSettings:ApiKey");
        }

        /** Permet de retrouver la listes des actualités de jeux. */
        public List<Article> GetGameNews()
        {
            try
            {
                // Appel à une url pour l'api externe d'actualités.
                string data = new WebClient().DownloadString(apiUrl);
                // Conversion : format json -> Objet "News".
                News news = JsonConvert.DeserializeObject<News>(data);
                // Suppression des articles inutiles ou incomplet.
                for (int i = 0; i < news.Articles.Count; i++)
                {
                    if (news.Articles[i].Description.StartsWith("Comments"))
                        news.Articles.RemoveAt(i);
                }
                // On retourne uniquement les articles restants.
                return news.Articles;
            }
            catch (Exception) { return new List<Article>(); }
        }
    }
}