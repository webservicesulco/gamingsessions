﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Core.Interfaces.DataBaseRepository;
using gamingsessions.Entities.Models.DataBaseRepository;

namespace gamingsessions.Business.Services
{
    /** Classe permettant de gérer les utilisateurs. */
    public class UserService : IUserService
    {
        private readonly IUserRepository userRepository;

        public UserService(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        /** Fonction de récupération d'un utilisateur à partir de son identifiant OAuth2. */
        public User RetrieveByOAuthID(string oAuthID)
        {
            return userRepository.SelectByOAuthID(oAuthID);
        }

        /** Fonction de création d'un utilisateur. */
        public User Create(User user)
        {
            return userRepository.Insert(user);
        }
    }
}