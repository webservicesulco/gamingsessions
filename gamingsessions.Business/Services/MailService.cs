﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Entities.Models.Business;
using gamingsessions.Entities.Models.DataBaseRepository;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace gamingsessions.Business.Services
{
    /* Classe permettant d'envoyer des mails. */
    public class MailService : IMailService
    {
        private readonly MailSettings mailSettings;
        private readonly IUserService userService;
        private readonly ISessionService sessionService;

        public MailService(IOptions<MailSettings> mailSettings, IUserService userService, ISessionService sessionService)
        {
            this.mailSettings = mailSettings.Value;
            this.userService = userService;
            this.sessionService = sessionService;
        }

        /** Fonction d'envoi de mail. */
        public async Task SendEmailAsync(int idSession, string oAuthID)
        {
            // Récupération de la session concernée par l'envoi du mail.
            Session session = sessionService.Retrieve(idSession);
            // Récupération de l'utilisateur concernée par la réception du mail.
            User user = userService.RetrieveByOAuthID(oAuthID);
            // Création du modèle de mail selon les informations de session et utilisateur.
            Mail mail = CreateMail(session, user);

            MimeMessage email = new MimeMessage();

            // Ajout des informations relatives au mail (expéditeur, destinataire, sujet, corps).
            email.From.Add(new MailboxAddress(mailSettings.DisplayName, mailSettings.Mail));
            email.To.Add(MailboxAddress.Parse(mail.ToEmail));
            email.Subject = mail.Subject;
            var builder = new BodyBuilder
            {
                HtmlBody = mail.HTMLBody,
                TextBody = mail.Body
            };
            email.Body = builder.ToMessageBody();

            // Création du client de serveur SMTP.
            using var smtp = new SmtpClient();
            // Connexion au serveur smtp.
            smtp.Connect(mailSettings.Host, mailSettings.Port, true);
            // Authentification auprès du serveur smtp.
            smtp.Authenticate(mailSettings.Mail, mailSettings.Password);
            // Envoi du mail.
            await smtp.SendAsync(email);
            // Déconnexion.
            smtp.Disconnect(true);
            smtp.Dispose();
        }

        /** Fontion de création de l'objet Mail */
        private Mail CreateMail(Session session, User user)
        {
            Mail mail = new Mail
            {
                ToEmail = user.Email,
                Subject = "Gaming Sessions : Rappel session de " + session.GameName
            };
            // Mise en forme de la date.
            string date = DateTime.Parse(session.SessionDate).ToString("dddd dd MMMM yyyy à HH:mm", CultureInfo.CreateSpecificCulture("fr-FR"));
            // Écriture du corps du mail.
            mail.Body = "Bonjour,\nCeci est un message généré automatiquement par l'application GamingSessions.\nMerci de ne pas y répondre.\n\nVous avez une session de jeu prévue sur "
                            + session.GameName + " (" + session.PlatForm + "), le "
                            + date
                            + ".\nCette session a été organisée par "
                            + session.Creator.UserName
                            + ".";
            mail.HTMLBody = "<p>Bonjour,<br>Ceci est un message généré automatiquement par l'application GamingSessions.<br>Merci de ne pas y répondre.<br><br>Vous avez une session de jeu prévue sur "
                            + session.GameName + " (" + session.PlatForm + "), le "
                            + date
                            + ".<br>Cette session a été organisée par "
                            + session.Creator.UserName
                            + ".";
            return mail;
        }
    }
}