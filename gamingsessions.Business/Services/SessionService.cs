﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Core.Interfaces.DataBaseRepository;
using gamingsessions.Entities.Models.DataBaseRepository;
using System;
using System.Collections.Generic;

namespace gamingsessions.Business.Services
{
    /** Classe permettant de la création, modification et suppression d'une session. */
    public class SessionService : ISessionService
    {
        private readonly ISessionRepository sessionRepository;
        private readonly IUserService userService;

        public SessionService(ISessionRepository sessionRepository, IUserService userService)
        {
            this.sessionRepository = sessionRepository;
            this.userService = userService;
        }

        /** Permet de retrouver la session avec un param name="id". */
        public Session Retrieve(int id)
        {
            return sessionRepository.Select(id);
        }

        /** Permet de récupérer toutes les sessions. */
        public IEnumerable<Session> RetrieveAll()
        {
            return sessionRepository.SelectAll();
        }

        /** Permet de modifier une session. */
        public Session Update(int id, Session session)
        {
            return sessionRepository.Update(id, session);
        }

        /** Permet de créer une session. */
        public Session Create(string oAuthID, Session session)
        {
            try
            {
                // Récupére un utilisateur en fonction de son identifiant oAuth2.
                User user = userService.RetrieveByOAuthID(oAuthID);
                // Insére l'utilisateur en tant que créateur.
                session.Creator = user;
                // Récupére la date du jour au format "MM/dd/yyyy HH:mm:ss".
                session.CreationDate = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
                return sessionRepository.Insert(session);
            }
            catch (Exception) { return null; }
        }

        /** Permet de supprimer la session avec un id. */
        public bool Remove(int idSession)
        {
            return sessionRepository.Delete(idSession);
        }

        /** Permet de supprimer les sessions périmées. */
        public bool RemoveOutDate()
        {
            return sessionRepository.DeleteOutDate();
        }
    }
}