﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Entities.Models.DataBaseRepository;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3.Data;
using Google.Apis.Services;
using System;

namespace gamingsessions.Business.Services
{
    /** Classe permettant de créer et envoyer les events Calendrier de Google. */
    public class CalendarService : ICalendarService
    {
        private readonly ISessionService sessionService;

        public CalendarService(ISessionService sessionService)
        {
            this.sessionService = sessionService;
        }

        /** Permet de créer un evénement de Calendrier Google.  */
        public bool CreateEvent(GoogleCredential cred, int idSession)
        {
            try
            {
                // Récupération de la session concernée par l'événement.
                Session session = this.sessionService.Retrieve(idSession);
                // Récupération de la date.
                DateTime dateTime = DateTime.Parse(session.SessionDate);
                // Création du modèle d'évenement.
                Entities.Models.Business.Calendar calendarModel = new Entities.Models.Business.Calendar()
                {
                    Summary = ("gamingsessions : Session de " + session.GameName + " prévue."),
                    Start = dateTime,
                    End = dateTime.AddHours(2)
                };
                // Appel au service Google Calendar (nécessite les credentials Google).
                var service = new Google.Apis.Calendar.v3.CalendarService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = cred,
                    ApplicationName = "gamingsessions"
                });
                // Création de l'évenement Google Calendar.
                Event newEvent = new Event()
                {
                    Summary = calendarModel.Summary,
                    Start = new EventDateTime() { DateTime = calendarModel.Start },
                    End = new EventDateTime() { DateTime = calendarModel.End }
                };
                // Insertion de l'évenement.
                newEvent = service.Events.Insert(newEvent, "primary").Execute();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}