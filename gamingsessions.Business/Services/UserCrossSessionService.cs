﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Core.Interfaces.DataBaseRepository;
using gamingsessions.Entities.Models.DataBaseRepository;
using System;
using System.Collections.Generic;

namespace gamingsessions.Business.Services
{
    /** Classe permettant d'assigner un utilisateur à une session. */
    public class UserCrossSessionService : IUserCrossSessionService
    {
        private readonly ISessionService sessionService;
        private readonly IUserService userService;
        private readonly IUserCrossSessionRepository userCrossSessionRepository;

        public UserCrossSessionService(ISessionService sessionService, IUserService userService, IUserCrossSessionRepository userCrossSessionRepository)
        {
            this.sessionService = sessionService;
            this.userService = userService;
            this.userCrossSessionRepository = userCrossSessionRepository;
        }

        /** Fonction qui créer une liaison entre un utilisateur et une session. */
        public bool Create(string oAuthID, int idSession)
        {
            try
            {
                // Récupére un utilisateur par son nom = "userName".
                User user = userService.RetrieveByOAuthID(oAuthID);
                // Récupére une session par son identifiant = "idSession".
                Session session = sessionService.Retrieve(idSession);
                // Incrémente le compteur de joueur de 1.
                session.PlayerAccount += 1;
                // Met à jour la session.
                Session newSession = sessionService.Update(idSession, session);
                // Insére la session et l'utilisateur dans la table "userCrossSession".
                userCrossSessionRepository.Insert(user, newSession);
                return true;
            }
            catch (Exception) { return false; }
        }

        /** Récupére les sessions rejointes par un utilisateur. */
        public IEnumerable<int> GetSessionsByUser(string oAuthID)
        {
            // Récupére l'utilisateur par son authentification.
            User user = userService.RetrieveByOAuthID(oAuthID);
            // Retourne les sessions rejointes par l'utilisateur.
            return userCrossSessionRepository.SelectSessionsByUser(user);
        }

        /** Récupére les utilistateurs en fonction de l'id d'une session. */
        public IEnumerable<string> GetUsersBySession(int idSession)
        {
            // Récupére la session avec son id.
            Session session = sessionService.Retrieve(idSession);
            // Retourne tous les utilisateurs par sessions.
            return userCrossSessionRepository.SelectUsersBySession(session);
        }

        /** Fonction qui retire la laiaison entre l'utilisateur et la session passée en paramèrtre. */
        public bool Remove(string oAuthID, int idSession)
        {
            try
            {
                // Récupére l'utilisateur par son authentification.
                User user = userService.RetrieveByOAuthID(oAuthID);
                // Récupére la session avec son id.
                Session session = sessionService.Retrieve(idSession);
                // Décremente le compteur de joueur.
                session.PlayerAccount -= 1;
                // Met à jour la session.
                Session newSession = sessionService.Update(idSession, session);
                // Supprime la session et l'utilisateur dans la table "userCrossSession".
                userCrossSessionRepository.Delete(user, newSession);
                return true;
            }
            catch (Exception) { return false; }
        }

        /** Fonction qui retire les liaisons avec la session passée en paramètre. */
        public bool RemoveBySession(int idSession)
        {
            try
            {
                // Récupére la session avec son id.
                Session session = sessionService.Retrieve(idSession);
                // Supprime la session de la table "userCrossSession".
                return userCrossSessionRepository.DeleteBySession(session);
            }
            catch (Exception) { return false; }
        }

        /** Fonction qui retire les liaisons avec des sessions périmées. */
        public bool RemoveOutDate()
        {
            // Supprime les sessions périmées.
            return userCrossSessionRepository.DeleteOutDate();
        }
    }
}