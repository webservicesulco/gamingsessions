﻿using gamingsessions.Entities.Models.DataBaseRepository;
using Microsoft.EntityFrameworkCore;

namespace gamingsessions.DatabaseRepository.Context
{
    public class GamingSessionsContext : DbContext
    {
        public GamingSessionsContext(DbContextOptions<GamingSessionsContext> options) : base(options)
        {
        }

        public DbSet<Session> Sessions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserCrossSession> UsersCrossSessions { get; set; }
    }
}