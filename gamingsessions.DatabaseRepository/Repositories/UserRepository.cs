﻿using gamingsessions.Core.Interfaces.DataBaseRepository;
using gamingsessions.DatabaseRepository.Context;
using gamingsessions.Entities.Models.DataBaseRepository;
using System;
using System.Linq;

namespace gamingsessions.DatabaseRepository.Repositories
{
    /** Classe permettant de gérer les utilisateurs dans la base de données. */
    public class UserRepository : IUserRepository
    {
        private readonly GamingSessionsContext gamingSessionsContext;

        public UserRepository(GamingSessionsContext gamingSessionsContext)
        {
            this.gamingSessionsContext = gamingSessionsContext;
        }

        /** Permet de récupérer un utilisateur selon son ID.*/
        public User Select(int id)
        {
            try
            {
                return gamingSessionsContext.Users.Find(id);
            }
            catch (Exception) { return null; }
        }

        /** Permet de récupérer un utilisateur selon son ID OAuth2.*/
        public User SelectByOAuthID(string oAuthId)
        {
            try
            {
                return gamingSessionsContext.Users.Where(u => u.OAuthId == oAuthId).FirstOrDefault<User>();
            }
            catch (Exception) { return null; }
        }

        /** Permet d'ajouter un utilisateur dans la base de données. */
        public User Insert(User user)
        {
            try
            {
                gamingSessionsContext.Users.Add(user);
                gamingSessionsContext.SaveChanges();
                return user;
            }
            catch (Exception) { return null; }
        }
    }
}