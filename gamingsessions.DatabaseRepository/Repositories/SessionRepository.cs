﻿using gamingsessions.Core.Interfaces.DataBaseRepository;
using gamingsessions.DatabaseRepository.Context;
using gamingsessions.Entities.Models.DataBaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gamingsessions.DatabaseRepository.Repositories
{
    /** Classe permettant de récupérer ou changer des informations dans la base de données, relatives aux sessions. */
    public class SessionRepository : ISessionRepository
    {
        private readonly GamingSessionsContext gamingSessionsContext;

        public SessionRepository(GamingSessionsContext gamingSessionsContext)
        {
            this.gamingSessionsContext = gamingSessionsContext;
        }

        /** Fontion de récupération d'une session à partir de son id. */
        public Session Select(int id)
        {
            try
            {
                return gamingSessionsContext.Sessions.Include(a => a.Creator).Where(s => s.Id == id).SingleOrDefault();
            }
            catch (Exception) { return null; }
        }

        /** Fonction de récupération de toutes les sessions enregistrées. */
        public IEnumerable<Session> SelectAll()
        {
            try
            {
                return gamingSessionsContext.Sessions.Include(a => a.Creator);
            }
            catch (Exception) { return null; }
        }

        /** Fonction d'insertion d'une session. */
        public Session Insert(Session session)
        {
            try
            {
                gamingSessionsContext.Sessions.Add(session);
                gamingSessionsContext.SaveChanges();
                return session;
            }
            catch (Exception) { return null; }
        }

        /** Fonction de mise à jour des informations d'une session. */
        public Session Update(int id, Session session)
        {
            try
            {
                Session existingSession = this.Select(id);
                existingSession.PlatForm = session.PlatForm;
                existingSession.GameName = session.GameName;
                existingSession.PlayerAccount = session.PlayerAccount;
                existingSession.PlayerMaxAccount = session.PlayerMaxAccount;
                existingSession.SessionDate = session.SessionDate;
                gamingSessionsContext.SaveChanges();
                return existingSession;
            }
            catch (Exception) { return null; }
        }

        /** Permet de supprimer une session selon son ID. */
        public bool Delete(int idSession)
        {
            try
            {
                gamingSessionsContext.Sessions.RemoveRange(gamingSessionsContext.Sessions.Where(s => s.Id == idSession));
                gamingSessionsContext.SaveChanges();
                return true;
            }
            catch (Exception) { return false; }
        }

        /** Permet de supprimer les sessions obsolètes. */
        public bool DeleteOutDate()
        {
            try
            {
                gamingSessionsContext.Database.ExecuteSqlRaw("DELETE FROM Sessions WHERE (SessionDate < datetime('now'))");
                gamingSessionsContext.SaveChanges();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}