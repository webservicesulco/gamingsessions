﻿using gamingsessions.Core.Interfaces.DataBaseRepository;
using gamingsessions.DatabaseRepository.Context;
using gamingsessions.Entities.Models.DataBaseRepository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gamingsessions.DatabaseRepository.Repositories
{
    /** Classe permettant de gérer les liaisons utilisateurs - sessions dans la base de données. */
    public class UserCrossSessionRepository : IUserCrossSessionRepository
    {
        private readonly GamingSessionsContext gamingSessionsContext;

        public UserCrossSessionRepository(GamingSessionsContext gamingSessionsContext)
        {
            this.gamingSessionsContext = gamingSessionsContext;
        }

        /** Permet de récupérer les sessions rejointes par l'utilisateur concerné. */
        public IEnumerable<int> SelectSessionsByUser(User user)
        {
            try
            {
                // Création et éxecution de la requête.
                var query = gamingSessionsContext.Sessions
                            .Join(gamingSessionsContext.UsersCrossSessions,
                                session => session,
                                entry => entry.Session,
                                (session, entry) => new { SessionId = session.Id, User = entry.User })
                            .Where(returnValue => returnValue.User == user)
                            .Select(returnValue => returnValue.SessionId)
                            .ToList();
                // Si aucun résultat, liste vide.
                if (query.Count == 0)
                    return Enumerable.Empty<int>();
                else
                    return query;
            }
            catch (Exception) { return null; }
        }

        /** Permet de récupérer les utilisateurs ayant rejoint la session concernée. */
        public IEnumerable<string> SelectUsersBySession(Session session)
        {
            try
            {
                // Création et éxecution de la requête.
                var query = gamingSessionsContext.Users
                            .Join(gamingSessionsContext.UsersCrossSessions,
                                user => user,
                                entry => entry.User,
                                (user, entry) => new { Username = user.UserName, Session = entry.Session })
                            .Where(returnValue => returnValue.Session == session)
                            .Select(returnValue => returnValue.Username)
                            .ToList();
                // Si aucun résultat, liste vide.
                if (query.Count == 0)
                    return Enumerable.Empty<string>();
                else
                    return query;
            }
            catch (Exception) { return null; }
        }

        /** Insertion d'une liaison utilisateur - session.*/
        public bool Insert(User user, Session session)
        {
            try
            {
                // Ajout d'un objet de type UserCrossSession dans la table directement.
                gamingSessionsContext.UsersCrossSessions.Add(new UserCrossSession { User = user, Session = session });
                gamingSessionsContext.SaveChanges();
                return true;
            }
            catch (Exception) { return false; }
        }

        /** Permet de supprimer une liaison entre un utilisateur et une session concernée. */
        public bool Delete(User user, Session session)
        {
            try
            {
                // Suppression de(s) enregistrement(s) incluant l'utilisateur et la session concernée.
                gamingSessionsContext.UsersCrossSessions.RemoveRange(gamingSessionsContext.UsersCrossSessions
                    .Where(s => s.Session == session && s.User == user));
                gamingSessionsContext.SaveChanges();
                return true;
            }
            catch (Exception) { return false; }
        }

        /** Permet de supprimer toutes les liaisons utilisateurs - sessions attachée à une session donnée. */
        public bool DeleteBySession(Session session)
        {
            try
            {
                // Suppression de(s) enregistrement(s) incluant la session concernée.
                gamingSessionsContext.UsersCrossSessions.RemoveRange(gamingSessionsContext.UsersCrossSessions
                    .Where(s => s.Session == session));
                gamingSessionsContext.SaveChanges();
                return true;
            }
            catch (Exception) { return false; }
        }

        /** Permet de supprimer les liaisons utilisateurs - sessions obsolètes.*/
        public bool DeleteOutDate()
        {
            try
            {
                // Exécution d'une requête SQL "brute" permettant de prendre en charge les méthodes sqlite incluses.
                gamingSessionsContext.Database.ExecuteSqlRaw("DELETE FROM UsersCrossSessions WHERE UsersCrossSessions.SessionId IN (SELECT Sessions.Id FROM Sessions WHERE Sessions.SessionDate < datetime('now'))");
                gamingSessionsContext.SaveChanges();
                return true;
            }
            catch (Exception) { return false; }
        }
    }
}