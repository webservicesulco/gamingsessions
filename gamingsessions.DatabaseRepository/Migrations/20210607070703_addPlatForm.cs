﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace gamingsessions.DatabaseRepository.Migrations
{
    public partial class addPlatForm : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PlatForm",
                table: "Sessions",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PlatForm",
                table: "Sessions");
        }
    }
}