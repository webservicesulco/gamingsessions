using gamingsessions.Entities.Models.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using System;

namespace gamingsessions.Api.Controllers
{
    /** Controller pour l'authentification. */
    [Route("[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class SettingsController : Controller
    {
        private readonly IConfiguration configuration;
        private readonly IMemoryCache memoryCache;
        private readonly MemoryCacheEntryOptions cacheEntryOptions;

        public SettingsController(IConfiguration configuration, IMemoryCache memoryCache)
        {
            this.configuration = configuration;
            this.memoryCache = memoryCache;
            this.cacheEntryOptions = new MemoryCacheEntryOptions();
        }

        [HttpGet]
        [Route("auth")]
        public IActionResult Get()
        {
            try
            {
                PublicAuthSettings dto;
                bool alreadyExist = memoryCache.TryGetValue("cachedDto", out PublicAuthSettings cachedDto);
                if (!alreadyExist)
                {
                    dto = new PublicAuthSettings()
                    {
                        Audience = configuration.GetValue<string>("Auth:Audience"),
                        Domain = configuration.GetValue<string>("Auth:Domain"),
                        ClientId = configuration.GetValue<string>("Auth:ClientId")
                    };
                    cachedDto = dto;
                    memoryCache.Set("cachedDto", cachedDto, cacheEntryOptions);
                }
                return Ok(cachedDto);
            }
            catch (Exception)
            {
                return BadRequest("Erreur de récupération des informations Oauth2.");
            }
        }
    }
}