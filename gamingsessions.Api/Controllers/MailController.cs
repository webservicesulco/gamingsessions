﻿using gamingsessions.Core.Interfaces.Business;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace gamingsessions.Api.Controllers
{
    /** Controller pour l'api mail. */
    [Route("[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class MailController : ControllerBase
    {
        private readonly IMailService mailService;

        public MailController(IMailService mailService)
        {
            this.mailService = mailService;
        }

        [HttpPut("send/{idSession}")]
        public async Task<IActionResult> Send(int idSession)
        {
            try
            {
                string oAuthID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
                await mailService.SendEmailAsync(idSession, oAuthID);
                return Accepted();
            }
            catch (Exception)
            {
                return BadRequest("Erreur d'envoi de mail.");
            }
        }
    }
}