﻿using gamingsessions.Core.Interfaces.Business;
using Google.Apis.Auth.AspNetCore3;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Calendar.v3;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;

namespace gamingsessions.Api.Controllers
{
    /** Controlleur pour l'api Google Calendar. */
    [Route("[controller]")]
    [ApiController]
    public class CalendarController : ControllerBase
    {
        private readonly IConfiguration configuration;
        private readonly ICalendarService calendarService;

        public CalendarController(IConfiguration configuration, ICalendarService calendarService)
        {
            this.configuration = configuration;
            this.calendarService = calendarService;
        }

        /** Route de type GET :
         Cas spécial : cette route est appelée via une redirection de navigateur pour garantir la connexion à l'api Google. */
        [HttpGet]
        [Route("send/{idSession}")]
        [GoogleScopedAuthorize(CalendarService.ScopeConstants.Calendar)]
        public async Task<IActionResult> Send([FromServices] IGoogleAuthProvider auth, int idSession)
        {
            GoogleCredential cred = await auth.GetCredentialAsync();
            if (calendarService.CreateEvent(cred, idSession))
                return Redirect(configuration.GetValue<string>("clientUrl") + "/fetch-session");
            else
                return BadRequest("Erreur : Impossible de créer l'événement Google Calendar.");
        }
    }
}