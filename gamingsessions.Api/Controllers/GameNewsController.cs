﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Entities.Models.Business;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace gamingsessions.Api.Controllers
{
    /** Controller pour l'api news. */
    [Route("[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class GameNewsController : ControllerBase
    {
        private readonly IGameNewsService gameNewsService;

        public GameNewsController(IGameNewsService gameNewsService)
        {
            this.gameNewsService = gameNewsService;
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAll()
        {
            List<Article> articlesList = gameNewsService.GetGameNews();
            if (articlesList.Count > 0)
                return Ok(articlesList);
            else
                return BadRequest("Problème récupération news");
        }
    }
}