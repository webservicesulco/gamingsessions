﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Entities.Models.DataBaseRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;

namespace gamingsessions.Api.Controllers
{
    /** Controller pour les sessions. */
    [Route("[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class SessionsController : ControllerBase
    {
        private readonly ISessionService sessionService;

        public SessionsController(ISessionService sessionService)
        {
            this.sessionService = sessionService;
        }

        [HttpGet]
        [Route("{idSession}")]
        public IActionResult Get(int idSession)
        {
            Session session = sessionService.Retrieve(idSession);
            if (session != null)
                return Ok(session);
            else
                return NotFound("Session introuvable.");
        }

        [HttpGet]
        [Route("")]
        public IActionResult GetAll()
        {
            IEnumerable<Session> res = sessionService.RetrieveAll();
            if (res != null)
                return Ok(res);
            else
                return BadRequest("Erreur de récupération de toutes les sessions.");
        }

        [HttpPut]
        [Route("")]
        public IActionResult Put(Session session)
        {
            string oAuthID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            Session newSession = this.sessionService.Create(oAuthID, session);
            if (newSession != null)
                return Ok(newSession);
            else
                return BadRequest("Problème de création session.");
        }

        [HttpPost]
        [Route("")]
        public IActionResult Post(Session session)
        {
            Session newSession = sessionService.Update(session.Id, session);
            if (newSession != null)
                return Ok(newSession);
            else
                return BadRequest("Problème de mise à jour session.");
        }

        [HttpDelete]
        [Route("{idSession}")]
        public IActionResult Delete(int idSession)
        {
            if (sessionService.Remove(idSession))
                return NoContent();
            else
                return BadRequest("Impossible de supprimer cette session.");
        }

        [HttpDelete]
        [Route("outDate")]
        public IActionResult DeleteOutDate()
        {
            if (sessionService.RemoveOutDate())
                return NoContent();
            else
                return BadRequest("Impossible de supprimer cette session.");
        }
    }
}