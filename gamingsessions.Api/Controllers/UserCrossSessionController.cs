using gamingsessions.Core.Interfaces.Business;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Security.Claims;

namespace gamingsessions.Api.Controllers
{
    /** Controller pour la liaison entre utilisateur et session. */
    [Route("[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UserCrossSessionController : ControllerBase
    {
        private readonly IUserCrossSessionService userCrossSessionService;

        public UserCrossSessionController(IUserCrossSessionService userCrossSessionService)
        {
            this.userCrossSessionService = userCrossSessionService;
        }

        [HttpPut]
        [Route("{idSession}")]
        public IActionResult Put(int idSession)
        {
            string oAuthID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (userCrossSessionService.Create(oAuthID, idSession))
                return NoContent();
            else
                return BadRequest("Erreur : Impossible de rejoindre cette session.");
        }

        [HttpDelete]
        [Route("{idSession}")]
        public IActionResult Delete(int idSession)
        {
            string oAuthID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            if (userCrossSessionService.Remove(oAuthID, idSession))
                return NoContent();
            else
                return BadRequest("Impossible de quitter cette sessions.");
        }

        [HttpGet]
        [Route("byUser")]
        public IActionResult GetByUser()
        {
            string oAuthID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            IEnumerable<int> usersId = userCrossSessionService.GetSessionsByUser(oAuthID);
            if (usersId != null)
                return Ok(usersId);
            else
                return BadRequest("Impossible de r�cup�rer les utilisateurs associ�s � cette session.");
        }

        [HttpGet]
        [Route("bySession/{idSession}")]
        public IActionResult GetBySession(int idSession)
        {
            IEnumerable<string> sessions = userCrossSessionService.GetUsersBySession(idSession);
            if (sessions != null)
                return Ok(sessions);
            else
                return BadRequest("Impossible de r�cup�rer les sessions associ�es � cet utilisateur.");
        }

        [HttpDelete]
        [Route("bySession/{idSession}")]
        public IActionResult DeleteBySession(int idSession)
        {
            if (userCrossSessionService.RemoveBySession(idSession))
                return NoContent();
            else
                return BadRequest("Impossible de supprimer les entr�es de sessions appartennant � la session" + idSession);
        }

        [HttpDelete]
        [Route("outDate")]
        public IActionResult DeleteOutDate()
        {
            if (userCrossSessionService.RemoveOutDate())
                return NoContent();
            else
                return BadRequest("Impossible de supprimer les anciennes entr�es de sessions.");
        }
    }
}