﻿using gamingsessions.Core.Interfaces.Business;
using gamingsessions.Entities.Models.DataBaseRepository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace gamingsessions.Api.Controllers
{
    /** Controller pour les utilisateurs. */
    [Route("[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class UsersController : ControllerBase
    {
        private readonly IUserService userService;

        public UsersController(IUserService userService)
        {
            this.userService = userService;
        }

        [HttpGet]
        [Route("account")]
        public IActionResult GetUser()
        {
            string oAuthID = User.FindFirst(ClaimTypes.NameIdentifier)?.Value;
            User user = userService.RetrieveByOAuthID(oAuthID);
            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }

        [HttpPut]
        [Route("")]
        public IActionResult Put(User user)
        {
            User newUser = userService.Create(user);
            if (newUser != null)
                return Ok(newUser);
            else
                return BadRequest("Utilisateur incorrect.");
        }
    }
}